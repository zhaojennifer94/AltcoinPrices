package eu.uwot.fabio.altcoinprices;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;


public class SettingsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Switch currencySwitch;
    private Spinner periodSpinner;
    private String period;
    private final String DEFAULT_PERIOD = "60";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // load  user settings //
        prefs = getApplicationContext().getSharedPreferences("Settings", 0); // 0 for private mode
        editor = prefs.edit();
        String currency = prefs.getString("currency", "EUR");
        this.period = prefs.getString("period", DEFAULT_PERIOD);

        // currency switch //
        currencySwitch = findViewById(R.id.currency_switch);

        currencySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    currencySwitch.setChecked(true);
                    editor.putString("currency", "USD");
                    editor.apply();
                } else {
                    currencySwitch.setChecked(false);
                    editor.putString("currency", "EUR");
                    editor.commit();
                }
            }
        });

        if (currency.equals("EUR")) {
            currencySwitch.setChecked(false);
        } else {
            currencySwitch.setChecked(true);
        }

        // set candlestick period //
        periodSpinner = findViewById(R.id.period_spinner);
        // Create an ArrayAdapter using the string array and a default altcoinNameSpinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.period_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the altcoinNameSpinner
        periodSpinner.setAdapter(adapter);
        periodSpinner.setOnItemSelectedListener(this);

        // Preload spinner with current period setting
        String spinnerLabel = periodToLabel(period);
        int spinnerPosition = adapter.getPosition(spinnerLabel);
        periodSpinner.setSelection(spinnerPosition);

        // about text - makes links clicable //
        TextView aboutText = findViewById(R.id.about);
        aboutText.setMovementMethod(LinkMovementMethod.getInstance());
        aboutText.setText(R.string.about_text);
        aboutText.append(" " + BuildConfig.VERSION_NAME);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        period = periodSpinner.getSelectedItem().toString();

        if ("1 minute".equals(period)) {
            period = "1";

        } else if ("3 minutes".equals(period)) {
            period = "3";

        } else if ("5 minutes".equals(period)) {
            period = "5";

        } else if ("15 minutes".equals(period)) {
            period = "15";

        } else if ("30 minutes".equals(period)) {
            period = "30";

        } else if (("1 " + getResources().getString(R.string.hour)).equals(period)) {
            period = "60";

        } else if (("2 " + getResources().getString(R.string.hours)).equals(period)) {
            period = "120";

        } else if (("3 " + getResources().getString(R.string.hours)).equals(period)) {
            period = "180";

        } else if (("4 " + getResources().getString(R.string.hours)).equals(period)) {
            period = "240";

        } else if (("1 " + getResources().getString(R.string.day)).equals(period)) {
            period = "D";

        } else if (("1 " + getResources().getString(R.string.week)).equals(period)) {
            period = "W";

        } else if (("1 " + getResources().getString(R.string.month)).equals(period)) {
            period = "M";

        } else {
            period = DEFAULT_PERIOD;

        }
        
        editor.putString("period", period);
        editor.commit();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // do nothing
    }

    private String periodToLabel(String period) {
        String label;

        switch (period) {
            case "1":
                label = "1 minute";
                break;
            case "3":
                label = "3 minutes";
                break;
            case "5":
                label = "5 minutes";
                break;
            case "15":
                label = "15 minutes";
                break;
            case "30":
                label = "30 minutes";
                break;
            case "60":
                label = "1 " + getResources().getString(R.string.hour);
                break;
            case "120":
                label = "2 " + getResources().getString(R.string.hours);
                break;
            case "180":
                label = "3 " + getResources().getString(R.string.hours);
                break;
            case "240":
                label = "4 " + getResources().getString(R.string.hours);
                break;
            case "D":
                label = "1 " + getResources().getString(R.string.day);
                break;
            case "W":
                label = "1 " + getResources().getString(R.string.week);
                break;
            case "M":
                label = "1 " + getResources().getString(R.string.month);
                break;
            default:
                label = DEFAULT_PERIOD;
                break;
        }

        return label;
    }
}
