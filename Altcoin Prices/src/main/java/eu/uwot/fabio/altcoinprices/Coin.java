package eu.uwot.fabio.altcoinprices;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Coin {

    final String[] coins = new String[] {
            "BTC",
            "BCH",
            "ETH",
            "LTC",

            "REP",
            "BAT",
            "BTG",
            "BLK",
            "ADA",
            "DASH",
            "DOGE",
            "EOS",
            "ETC",
            "GNO",
            "ICX",
            "ICN",
            "IOT",
            "LSK",
            "MLN",
            "XMR",
            "NANO",
            "XEM",
            "NEO",
            "OMG",
            "PART",
            "QTUM",
            "XRP",
            "XLM",
            "STRAT",
            "TRX",
            "VEN",
            "XVG",
            "ZEC",

            "BLX",
            "CCC",
            "SOPR",
            "FCI",
            "GEM",
            "PCC",
            "WMX",
            "BMC",
            "AAAX",
            "CAR",
            "KCOR",
            "CBST",
            "RCAA",
            "BIF",
            "CRNC",
            "BGA15",
            "BEA",
            "BLS",
            "FCE",
            "PPI",
            "JJK",
            "TRADE",
            "EAA",
            "SCX",
            "CCP"
    };
    private final String[] descriptions = new String[]{
            "Bitcoin",
            "Bitcoin Cash",
            "Ethereum",
            "Litecoin",

            "Augur",
            "Basic Attention Token",
            "Bitcoin Gold",
            "BlackCoin",
            "Cardano",
            "Dash",
            "Dogecoin",
            "EOS",
            "Ethereum Cash",
            "Gnosis",
            "Icon",
            "Iconomi",
            "IOTA",
            "Lisk",
            "Melon",
            "Monero",
            "Nano",
            "NEM",
            "NEO",
            "OmiseGO",
            "Particl",
            "Qtum",
            "Ripple",
            "Stellar Lumens",
            "Stratis",
            "TRON",
            "VeChain",
            "Verge",
            "Zcash",

            "Blockchain Index (ICN DAA)",
            "Crush Crypto Core (ICN DAA)",
            "Solidum Prime (ICN DAA)",
            "Future Chain Index (ICN DAA)",
            "Greychain Emerging Markets (ICN DAA)",
            "The Pecunio Cryptocurrency (ICN DAA)",
            "William Mougayar High Growth Cryptoassets Index (ICN DAA)",
            "BMC Original (ICN DAA)",
            "The Asymmetry DAA (ICN DAA)",
            "CARUS-AR (ICN DAA)",
            "KryptoStar CORE (ICN DAA)",
            "Coinbest 1 (ICN DAA)",
            "Ragnarok Crypto Asset Array (ICN DAA)",
            "Blockchain Infrastructure Index (ICN DAA)",
            "Cornucopia Index (ICN DAA)",
            "Global Blockchain Arrays represents (ICN DAA)",
            "Blockchain Easy Access (ICN DAA)",
            "Blockchain Smart (ICN DAA)",
            "Future Crypto Economy (ICN DAA)",
            "Phoenix Paradigm Indicator (ICN DAA)",
            "JJK Crypto Assets (ICN DAA)",
            "Trade (ICN DAA)",
            "Exponential Age Array (ICN DAA)",
            "StrongCoindex (ICN DAA)",
            "Pinta (ICN DAA)"
    };
    String[] coinsLabelDescriptionsString;
    Hashtable<String, String> coinsLabelDescriptionHashtable;
    Hashtable<String, String> coinsDescriptionLabelHashtable;
    final Hashtable<String, String> coinsLabelExchangeHashtable = new Hashtable<String, String>()
    {{  put("BTCUSD", "coinbase");
        put("ETHUSD", "coinbase");

        put("BTC", "coinbase");
        put("BCH", "coinbase");
        put("ETH", "coinbase");
        put("LTC", "coinbase");

        put("REP", "bittrex");
        put("BAT", "bittrex");
        put("BTG", "bittrex");
        put("BLK", "bittrex");
        put("ADA", "bittrex");
        put("DASH", "bittrex");//*
        put("DOGE", "bittrex");
        put("EOS", "bitfinex");
        put("ETC", "bittrex");
        put("GNO", "bittrex");
        put("ICX", "binance");
        put("ICN", "binance");
        put("IOT", "bitfinex");
        put("LSK", "bittrex");
        put("MLN", "bittrex");
        put("XMR", "bittrex");
        put("NANO", "binance");
        put("XEM", "bittrex");
        put("NEO", "bittrex");
        put("OMG", "bittrex");
        put("PART", "bittrex");
        put("QTUM", "bittrex");
        put("XRP", "bittrex");
        put("XLM", "bittrex");
        put("STRAT", "bittrex");
        put("TRX", "bitfinex");
        put("VEN", "binance");
        put("XVG", "bittrex");
        put("ZEC", "bittrex");

        put("BLX", "cryptocompare");
        put("CCC", "cryptocompare");
        put("SOPR", "cryptocompare");
        put("FCI", "cryptocompare");
        put("GEM", "cryptocompare");
        put("PCC", "cryptocompare");
        put("WMX", "cryptocompare");
        put("BMC", "cryptocompare");
        put("AAAX", "cryptocompare");
        put("CAR", "cryptocompare");
        put("KCOR", "cryptocompare");
        put("CBST", "cryptocompare");
        put("RCAA", "cryptocompare");
        put("BIF", "cryptocompare");
        put("CRNC", "cryptocompare");
        put("BGA15", "cryptocompare");
        put("BEA", "cryptocompare");
        put("BLS", "cryptocompare");
        put("FCE", "cryptocompare");
        put("PPI", "cryptocompare");
        put("JJK", "cryptocompare");
        put("TRADE", "cryptocompare");
        put("EAA", "cryptocompare");
        put("SCX", "cryptocompare");
        put("CCP", "cryptocompare");
    }};
    final Hashtable<String, String> coinsLabelGraph = new Hashtable<String, String>()
    {{  put("BTCUSD", "USD");
        put("ETHUSD", "USD");

        put("BTC", "free");
        put("BCH", "free");
        put("ETH", "free");
        put("LTC", "free");

        put("REP", "USD");
        put("BAT", "USD");
        put("BTG", "USDT");
        put("BLK", "USD");
        put("ADA", "USDT");
        put("DASH", "USDT");
        put("DOGE", "USD");
        put("EOS", "USD");
        put("ETC", "USDT");
        put("GNO", "USD");
        put("ICX", "USD");
        put("ICN", "BTC");
        put("IOT", "USD");
        put("LSK", "USD");
        put("MLN", "USD");
        put("XMR", "USDT");
        put("NANO", "BTC");
        put("XEM", "USD");
        put("NEO", "USDT");
        put("OMG", "USDT");
        put("PART", "USD");
        put("QTUM", "USD");
        put("XRP", "USDT");
        put("XLM", "USD");
        put("STRAT", "USD");
        put("TRX", "USD");
        put("VEN", "USD");
        put("XVG", "USDT");
        put("ZEC", "USDT");

        put("BLX", "cryptocompare");
        put("CCC", "na");
        put("SOPR", "na");
        put("FCI", "na");
        put("GEM", "na");
        put("PCC", "na");
        put("WMX", "na");
        put("BMC", "na");
        put("AAAX", "na");
        put("CAR", "na");
        put("KCOR", "na");
        put("CBST", "na");
        put("RCAA", "na");
        put("BIF", "na");
        put("CRNC", "na");
        put("BGA15", "na");
        put("BEA", "na");
        put("BLS", "na");
        put("FCE", "na");
        put("PPI", "na");
        put("JJK", "na");
        put("TRADE", "na");
        put("EAA", "na");
        put("SCX", "na");
        put("CCP", "na");
    }};


    private final Context context;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private float btcusd;
    private float btceur;
    private float usdeur;
    private float eurusd;

    public Coin(Context context) {
        this.context = context.getApplicationContext();
        initCoins();
    }

    public Coin(Context context, boolean doBTCUSDEUR) {
        this.context = context.getApplicationContext();
        prefs = context.getSharedPreferences("Settings", 0); // 0 for private mode
        btcusd = prefs.getFloat("btcusd", 1);
        btceur = prefs.getFloat("btceur", 1);
        usdeur = btceur / btcusd;
        eurusd = btcusd / btceur;
        initCoins();
    }

    private void initCoins() {
        coinsLabelDescriptionsString = new String[coins.length];
        for (int i = 0; i < coins.length; i++) {
            coinsLabelDescriptionsString[i] = coins[i] + " - " + descriptions[i];
        }

        coinsLabelDescriptionHashtable = new Hashtable<>();
        for (int i = 0; i < coins.length; i++) {
            coinsLabelDescriptionHashtable.put(coins[i], descriptions[i]);
        }

        coinsDescriptionLabelHashtable = new Hashtable<>();
        coinsDescriptionLabelHashtable.put("BTC/USD - Bitcoin", "BTCUSD");
        coinsDescriptionLabelHashtable.put("ETH/USD - Ethereum", "ETHUSD");
        for (int i = 0; i < coins.length; i++) {
            coinsDescriptionLabelHashtable.put(coins[i] + " - " + descriptions[i], coins[i]);
        }
    }

    public boolean addItem(String altcoinDesc, float amountBought, float unitPrice, String currency) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0); // 0 for private mode
        editor = prefs.edit();

        String altcoinLabel = coinsDescriptionLabelHashtable.get(altcoinDesc);
        float amountBought_old = prefs.getFloat(altcoinLabel + "_a", -1f);

        if (amountBought_old != -1f) {
            String altcoinCurrency = prefs.getString(altcoinLabel + "_currency", "USD");
            float unitPrice_old = prefs.getFloat(altcoinLabel + "_p", -1f);

            // Amount can't be a negative number
            if (amountBought_old + amountBought > 0) {
                // Convert value to the correct currency if needed
                if (!altcoinCurrency.equals(currency)) {
                    unitPrice = currencyToCurrency(unitPrice, altcoinCurrency);
                }

                unitPrice = (amountBought_old * unitPrice_old + amountBought * unitPrice) / (amountBought_old + amountBought);
                amountBought = amountBought_old + amountBought;
            } else {
                Log.d("DEBUG", "Portfolio Item can't contain a zero/negative amount of coins");
                return false;
            }
        } else {
            editor.putString(altcoinLabel + "_currency", currency);
        }

        editor.putFloat(altcoinLabel + "_a", amountBought);
        editor.putFloat(altcoinLabel + "_p", unitPrice);
        editor.apply();

        return true;
    }

    public boolean editItem(String altcoinName, float amountBought, float unitPrice) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0); // 0 for private mode
        editor = prefs.edit();

        if ((amountBought >= 0) && (unitPrice >= 0)) {
            editor.putFloat(altcoinName + "_a", amountBought);
            editor.putFloat(altcoinName + "_p", unitPrice);
            editor.apply();
        } else {
            Log.d("DEBUG", "Portfolio Item can't contain a zero/negative amount of coins");
            return false;
        }

        return true;
    }

    public void removeItem(String altcoinName) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0); // 0 for private mode
        editor = prefs.edit();

        editor.remove(altcoinName + "_a");
        editor.remove(altcoinName + "_p");
        editor.remove(altcoinName + "_currentBalance");
        editor.remove(altcoinName + "_currency");
        editor.apply();
    }

    // Get current coin value from exchange //
    public float getCurrentCoinValue(String altcoinName, String currency) {
        String exchange = getCoinExchange(altcoinName);
        float coinValue;

        switch (exchange) {
            /*case "KRAKEN":
                coinValue = getCoinQuoteKraken(altcoinName, currency);
                break;*/
            case "coinbase":
                prefs = context.getSharedPreferences("Settings", 0); // 0 for private mode
                currency = prefs.getString(altcoinName + "_currency", "EUR");
                coinValue = getCoinQuoteCoinbase(altcoinName, currency);
                break;
            case "bittrex":
                coinValue = getCoinQuoteBittrex(altcoinName, "BTC");
                break;
            case "binance":
                coinValue = getCoinQuoteBinance(altcoinName, "BTC");
                break;
            case "bitfinex":
                coinValue = getCoinQuoteBitfinex(altcoinName, "BTC");
                break;
            case "cryptocompare":
                coinValue = getCoinQuoteCryptoCompare(altcoinName, "BTC");
                break;
            default:
                coinValue = -1f;
                break;
        }

        // Exchange API are down or reporting broken values
        if (coinValue == -1f) {
            coinValue = getCoinInitialValue(altcoinName);
        }

        return coinValue;
    }

    // Get which exchange trade a coin //
    private String getCoinExchange (String altcoinName) {
        return coinsLabelExchangeHashtable.get(altcoinName);
    }

    // Get coin change from coinbase.com //
    // REQUEST: https://api.coinbase.com/v2/prices/BTC-USD/spot
    // RESPONSE: {"data":{"base":"BTC","currency":"USD","amount":"13857.00"}}
    public float getCoinQuoteCoinbase(String altcoinName, String currency) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String dataSTR = "";

        try {
            url = new URL("https://api.coinbase.com/v2/prices/" +
                                altcoinName +
                                "-" +
                                currency +
                                "/spot");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            assert url != null;
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(5000); // 5 seconds timeout

            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);

            int data = isw.read();
            while (data != -1) {
                char current = (char) data;
                data = isw.read();
                dataSTR = dataSTR + Character.toString(current);}
        } catch (SocketTimeoutException e) {
            dataSTR = "0";
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert urlConnection != null;
            urlConnection.disconnect();
        }

        Pattern pattern = Pattern.compile("([0-9]+).([0-9]+)");
        Matcher matcher = pattern.matcher(dataSTR);
        float coinQuote = -1f;
        if (matcher.find()) {
            coinQuote = Float.parseFloat(matcher.group(0));
        }

        return coinQuote;
    }

    // Get coin change in BTC from bittrex.com //
    // REQUEST: https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc
    private float getCoinQuoteBittrex(String altcoinName, String currency) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String dataSTR = "";

        try {
            url = new URL("https://bittrex.com/api/v1.1/public/getmarketsummary?market=" +
                    currency +
                    "-" +
                    altcoinName);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            assert url != null;
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(5000); // 5 seconds timeout

            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);

            int data = isw.read();
            while (data != -1) {
                char current = (char) data;
                data = isw.read();
                dataSTR = dataSTR + Character.toString(current);}
        } catch (SocketTimeoutException e) {
            dataSTR = "0";
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert urlConnection != null;
            urlConnection.disconnect();
        }

        Pattern pattern = Pattern.compile("Last\":([0-9]+).([0-9]+)");
        Matcher matcher = pattern.matcher(dataSTR);
        float coinQuote = -1f;
        if (matcher.find()) {
            pattern = Pattern.compile("([0-9]+).([0-9]+)");
            Matcher matcher1 = pattern.matcher(matcher.group(0));
            if (matcher1.find()) {
                //Log.d("DEBUG", "matcher1.group(0): " + matcher1.group(0));
                coinQuote = Float.parseFloat(matcher1.group(0));
            } else {
                Log.d("DEBUG", "matcher1 - Nothing was found");
            }
        } else {
            Log.d("DEBUG", "matcher - Nothing was found");
        }

        // Convert coinQuote(BTC) in currency
        if (coinQuote != -1f) {
            String newCurrency = prefs.getString(altcoinName + "_currency", "EUR");
            coinQuote = btcToCurrency(coinQuote, newCurrency);
        }

        return coinQuote;
    }

    // Get coin change in BTC from binance.com //
    // REQUEST: https://api.binance.com/api/v3/ticker/price?symbol=XMRBTC
    // RESPONSE: {"symbol":"XMRBTC","price":"0.02758400"}
    private float getCoinQuoteBinance(String altcoinName, String currency) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String dataSTR = "";

        try {
            url = new URL("https://api.binance.com/api/v3/ticker/price?symbol=" +
                    altcoinName +
                    currency);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            assert url != null;
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(5000); // 5 seconds timeout

            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);

            int data = isw.read();
            while (data != -1) {
                char current = (char) data;
                data = isw.read();
                dataSTR = dataSTR + Character.toString(current);}
        } catch (SocketTimeoutException e) {
            dataSTR = "0";
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert urlConnection != null;
            urlConnection.disconnect();
        }

        Pattern pattern = Pattern.compile("price\":\"([0-9]+).([0-9]+)\"");
        Matcher matcher = pattern.matcher(dataSTR);
        float coinQuote = -1f;
        if (matcher.find()) {
            pattern = Pattern.compile("([0-9]+).([0-9]+)");
            Matcher matcher1 = pattern.matcher(matcher.group(0));
            if (matcher1.find()) {
                //Log.d("DEBUG", "matcher1.group(0): " + matcher1.group(0));
                coinQuote = Float.parseFloat(matcher1.group(0));
            } else {
                Log.d("DEBUG", "matcher1 - Nothing was found");
            }
        } else {
            Log.d("DEBUG", "matcher - Nothing was found");
        }

        // Convert coinQuote(BTC) in currency
        if (coinQuote != -1f) {
            String newCurrency = prefs.getString(altcoinName + "_currency", "EUR");
            coinQuote = btcToCurrency(coinQuote, newCurrency);
        }

        return coinQuote;
    }

    // Get coin change in BTC from bitfinex.com //
    // REQUEST: https://api.bitfinex.com/v1/pubticker/btcusd
    // RESPONSE: {"mid":"2.0005","bid":"1.9974","ask":"2.0036","last_price":"1.9981","low":"1.9556","high":"2.1765","volume":"9717611.41918432","timestamp":"1518784863.8434322"}
    private float getCoinQuoteBitfinex(String altcoinName, String currency) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String dataSTR = "";

        try {
            url = new URL("https://api.bitfinex.com/v1/pubticker/" +
                    altcoinName +
                    currency);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            assert url != null;
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(5000); // 5 seconds timeout

            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);

            int data = isw.read();
            while (data != -1) {
                char current = (char) data;
                data = isw.read();
                dataSTR = dataSTR + Character.toString(current);}
        } catch (SocketTimeoutException e) {
            dataSTR = "0";
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert urlConnection != null;
            urlConnection.disconnect();
        }

        Pattern pattern = Pattern.compile("\"mid\":\"([0-9]+).([0-9]+)\"");
        Matcher matcher = pattern.matcher(dataSTR);
        float coinQuote = -1f;
        if (matcher.find()) {
            pattern = Pattern.compile("([0-9]+).([0-9]+)");
            Matcher matcher1 = pattern.matcher(matcher.group(0));
            if (matcher1.find()) {
                //Log.d("DEBUG", "matcher1.group(0): " + matcher1.group(0));
                coinQuote = Float.parseFloat(matcher1.group(0));
            } else {
                Log.d("DEBUG", "matcher1 - Nothing was found");
            }
        } else {
            Log.d("DEBUG", "matcher - Nothing was found");
        }

        // Convert coinQuote(BTC) in currency
        if (coinQuote != -1f) {
            String newCurrency = prefs.getString(altcoinName + "_currency", "EUR");
            coinQuote = btcToCurrency(coinQuote, newCurrency);
        }

        return coinQuote;
    }

    // Get coin change in BTC from cryptocompare.com //
    // REQUEST: https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD
    // RESPONSE: {"USD":6945.12}
    private float getCoinQuoteCryptoCompare(String altcoinName, String currency) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String dataSTR = "";
        float coinQuote = -1f;

        if (coinsLabelGraph.get(altcoinName).equals("cryptocompare")) {
            try {
                url = new URL("https://min-api.cryptocompare.com/data/price?fsym=" +
                        altcoinName +
                        "&tsyms=" +
                        currency);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            try {
                assert url != null;
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(5000); // 5 seconds timeout

                InputStream in = urlConnection.getInputStream();
                InputStreamReader isw = new InputStreamReader(in);

                int data = isw.read();
                while (data != -1) {
                    char current = (char) data;
                    data = isw.read();
                    dataSTR = dataSTR + Character.toString(current);
                }
            } catch (SocketTimeoutException e) {
                dataSTR = "0";
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                assert urlConnection != null;
                urlConnection.disconnect();
            }

            Pattern pattern = Pattern.compile("([0-9]+).([0-9]+)");
            Matcher matcher = pattern.matcher(dataSTR);

            if (matcher.find()) {
                coinQuote = Float.parseFloat(matcher.group(0));
            }

            // Convert coinQuote(BTC) in currency
            if (coinQuote != -1f) {
                String newCurrency = prefs.getString(altcoinName + "_currency", "EUR");
                coinQuote = btcToCurrency(coinQuote, newCurrency);
            }
        }

        return coinQuote;
    }

    // Convert price in BTC to EUR/USD //
    private float btcToCurrency(float coinQuote, String newCurrency) {
        switch (newCurrency) {
            case "USD":
                coinQuote *= btcusd;
                break;
            case "EUR":
                coinQuote *= btceur;
                break;
        }

        return coinQuote;
    }

    // Convert EUR in USD and vice versa //
    public float currencyToCurrency(float price, String newCurrency) {
        switch (newCurrency) {
            case "USD":
                price *= eurusd;
                break;
            case "EUR":
                price *= usdeur;
                break;
        }

        return price;
    }

    // Get the amount of FIAT a coin was paid //
    private float getCoinInitialValue(String altcoinName) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0); // 0 for private mode

        return prefs.getFloat(altcoinName + "_p", -1f);
    }

}
